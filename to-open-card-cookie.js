const { readFileSync, writeFileSync } = require('fs');

const cks = readFileSync('树叶面板Cookie列表.txt').toString();

writeFileSync(
  'open-card-cookie.txt',
  cks
    .replace(/Cookie\d+="/g, '')
    .replace(/".*/g, '')
    .split('\n')
    .join('&')
);
