const { join } = require('path');

// 互助码路径
const mutualCodePath = join('互助码.txt');

// 互助关系路径
const mutualRelationshipPath = join('互助关系.txt');

// 一些游戏名
const someGameName = {
  Fruit: '东东农场',
  Pet: '东东萌宠',
  Bean: '京东种豆得豆',
  DreamFactory: '京喜工厂',
  JdFactory: '东东工厂',
  Cash: '签到领现金',
  Cfd: '京喜财富岛',
};

// 有多少个京东账号
const jdAccountNumber = 15;

module.exports = {
  mutualCodePath,
  mutualRelationshipPath,
  someGameName,
  jdAccountNumber,
};
