const { readFileSync, writeFileSync } = require('fs');
const { generateAllMutualRelations } = require('./utils');
const {
  mutualCodePath,
  mutualRelationshipPath,
  someGameName,
  jdAccountNumber,
} = require('./config');

/**
 * 入口函数
 */
const main = () => {
  // 互助码数据
  const mutualCodeData = readFileSync(mutualCodePath).toString();

  // 生成所有互助关系
  const allMutualRelationshipBetween =
    generateAllMutualRelations(mutualCodeData, someGameName, jdAccountNumber) +
    '\n';

  // 所有互助关系，写到文件
  writeFileSync(mutualRelationshipPath, allMutualRelationshipBetween);
};

main();
