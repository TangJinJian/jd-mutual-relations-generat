# 京东互助关系生成

这个程序可以帮助你一次性的，把所提供的所有互助码，生成出互助关系。

# 使用方法

1. 安装 Node.js
2. 克隆这个仓库
3. 打开文件夹`jd-mutual-relations-generat`
4. 在`jd-mutual-relations-generat`文件夹下，创建一个文件`互助码.txt`
5. 打开控制面板，手动执行=》导出所有互助码
6. 复制所有的互助码
7. 粘贴到`互助码.txt`里，并保存
8. 在`jd-mutual-relations-generat`下执行命令`node index.js`
9. 生成的互助关系，就在`jd-mutual-relations-generat`文件夹下的`互助关系.txt`里
10. 复制`互助关系.txt`里的内容，然后翻到`config.sh`最末尾一行，粘贴即可
11. 点击`保存 config.sh`即可

